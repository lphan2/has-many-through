# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Patient.create(name: 'Mary Todd', street_address: '3842 Haven Port')
Patient.create(name: 'Elizabeth Minear', street_address: '9893 Alamana Bod')
Patient.create(name: 'Thomas Lindor', street_address: '5222 Peanuts House')
Patient.create(name: 'John Lemonades', street_address: '7877 Poultry Street')
Patient.create(name: 'Lindsay, T', street_address: '7263 Long Ave')
Patient.create(name: 'Matt, A', street_address: '20934 Ocean Lane')
Patient.create(name: 'Meg, R', street_address: '9021 Pacific Street')
Patient.create(name: 'Danny, K', street_address: '46832 Today Road')
Patient.create(name: 'Elliot, C', street_address: '27632 Forgetting Drive')
Specialist.create(name: 'Schema, A', specialty: 'Heart')
Specialist.create(name: 'Telle, C', specialty: 'Cancer')
Specialist.create(name: 'Aboran, K', specialty: 'Skin')
Specialist.create(name: 'Meggan, H', specialty: 'Blood')
Specialist.create(name: 'Lin, Y', specialty: 'Eyes')
Insurance.create(name:'Health corp', street_address:'9323 Janet Rd')
Insurance.create(name:'Sigta', street_address:'2322 Young Ave')
Insurance.create(name:'HPPO', street_address:'5677 Aveno St')
Insurance.create(name:'APPIED', street_address:'1211 Old Brick Lane')
Insurance.create(name:'TMHNG', street_address:'8702 Monthly Rd')