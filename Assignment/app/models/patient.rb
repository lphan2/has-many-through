class Patient < ActiveRecord::Base
  has_many :appointments
  belongs_to :insurance # foreign key - insurance_id
  has_many :specialists, through: :appointments
end
