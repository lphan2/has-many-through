class Appointment < ActiveRecord::Base
  belongs_to :patient # foreign key - patient_id
  belongs_to :specialist # foreign key - specialist_id
  validates_presence_of :fee
end
